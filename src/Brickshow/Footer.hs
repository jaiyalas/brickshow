module Brickshow.Footer where
--
import Brick.Types
import Brick.AttrMap (attrName)
import Brick.Widgets.Core
import Brick.Widgets.Center (center, vCenter)
--
--
footer :: Widget ()
footer =
  withAttr (attrName "footer") $
  -- hLimit 20 $
  vLimit 3 $
  center $
  vCenter $
  str "info!"
--

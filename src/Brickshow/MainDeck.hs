module Brickshow.MainDeck where
--
import Brick.Types
import Brick.AttrMap              (attrName)
import Brick.Widgets.Core
import Brick.Widgets.Center       (center, vCenter)
import Brick.Widgets.Border       (vBorder)
import Brick.Widgets.Border.Style (unicodeRounded)
--
--
mainDeck :: Widget ()
mainDeck =
    (center (str "Left of vertical border")
      <+> vBorder
      <+> center (str "Right of vertical border"))
 --

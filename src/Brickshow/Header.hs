{-# LANGUAGE OverloadedStrings #-}
module Brickshow.Header where
--
import Brick.Types
import Brick.AttrMap              (attrName)
import Brick.Widgets.Core
import Brick.Widgets.Center       (center, vCenter)
import Brick.Widgets.Border       (borderWithLabel)
import Brick.Widgets.Border.Style (unicodeRounded)
--
--
upperDeck :: Widget ()
upperDeck =
    withAttr (attrName "header") $
    hBox [demoBlock, demoBlock, demoBlock]
--
demoBlock :: Widget ()
demoBlock =
    withBorderStyle unicodeRounded $
    borderWithLabel (str "label") $
    vLimit 5 $
    vCenter $
    padLeftRight 2 $
    txt $ "title"
 --

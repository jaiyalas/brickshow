{-# LANGUAGE OverloadedStrings #-}
module Brickshow.Config where
--
import Graphics.Vty
--
import Brick.Util
import Brick.AttrMap
--
--
-- attrs :: [(A.AttrName, V.Attr)]
-- attrs =
--     [ (B.borderAttr,     yellow `on` black)
--     , (B.vBorderAttr,    fg blue)
--     , (B.hBorderAttr,    fg red)
--     , (attrName "title", fg green)
--     ]
--
globalDefault :: Attr
globalDefault = fg white -- white `on` black
--
attrConfig :: AttrMap
attrConfig = attrMap globalDefault
    [ (attrName "footer",  fg green)
    , (attrName "header",  green `on` black)
    , (attrName "general", blue `on` black)
    , (attrName "general"
        <> attrName "specific", fg cyan)
    , (attrName "linked",  fg yellow `withURL` "http://www.google.com/")
    ]
--

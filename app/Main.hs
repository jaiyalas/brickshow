{-# LANGUAGE CPP #-}
{-# LANGUAGE OverloadedStrings #-}
module Main where
--
-- #if !(MIN_VERSION_base(4,11,0))
import           Data.Monoid ((<>))
-- #endif
--
import qualified Data.Text     as T
import           Graphics.Vty  as V
--
import           Brick.Main    as M
import           Brick.Util
import           Brick.AttrMap as A
import           Brick.Types
import           Brick.Widgets.Core
import           Brick.Widgets.Center as C
import           Brick.Widgets.Border as B
import           Brick.Widgets.Border.Style as BS
--
import           Brickshow
--
--
ui :: Widget ()
ui = vBox [ upperDeck
          , B.hBorder
          , mainDeck
          , B.hBorder
          , lowerDeck
          ]
--
app :: App () e ()
app = App { appDraw = const [ui]
          , appHandleEvent = resizeOrQuit
          , appStartEvent = return ()
          , appAttrMap = const attrConfig
          , appChooseCursor = neverShowCursor
          }
--
main :: IO ()
main = defaultMain app ()
